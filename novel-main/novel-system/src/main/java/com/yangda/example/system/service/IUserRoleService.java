package com.yangda.example.system.service;

import com.yangda.example.interfaces.IBaseService;
import com.yangda.example.system.entity.UserRole;


public interface IUserRoleService extends IBaseService <UserRole,Integer>{

}
