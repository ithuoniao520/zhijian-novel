package com.yangda.example.system.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.yangda.example.entity.request.BaseRequest;

public class PermissionRequest extends BaseRequest {

	private Integer pid;
	private String name;
	private String type;
	private Integer sort;
	private String url;
	private String icon;
	private String permCode;
	private String state;
	protected Integer orderNo;
	@JSONField(name = "_parentId")
	private Integer _parentId;// easyiui中指定其父节点

	public Integer get_parentId() {
		return _parentId;
	}

	public void set_parentId(Integer _parentId) {
		this._parentId = _parentId;
	}

	// Constructors

	/** default constructor */
	public PermissionRequest() {
	}

	/** minimal constructor */
	public PermissionRequest(String name) {
		this.name = name;
	}

	public PermissionRequest(Integer pid, String name) {
		this.pid = pid;
		this.name = name;
	}

	public PermissionRequest(Integer pid, String name, String type, String url, String permCode) {
		this.pid = pid;
		this.name = name;
		this.type = type;
		this.url = url;
		this.permCode = permCode;
	}

	public Integer getPid() {
		return this.pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getSort() {
		return this.sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getPermCode() {
		return this.permCode;
	}

	public void setPermCode(String permCode) {
		this.permCode = permCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}

}