package com.yangda.example.system.service.impl;

import com.yangda.example.interfaces.IBaseDao;
import com.yangda.example.service.BaseService;
import com.yangda.example.system.dao.IRoleDao;
import com.yangda.example.system.entity.Role;
import com.yangda.example.system.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService extends BaseService<Role, Integer> implements IRoleService {

	@Autowired
	private IRoleDao roleDao;

	@Override
	public IBaseDao<Role, Integer> getEntityDao() {
		return roleDao;
	}
}
