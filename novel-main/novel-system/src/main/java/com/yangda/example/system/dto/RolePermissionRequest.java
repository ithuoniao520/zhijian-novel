package com.yangda.example.system.dto;

import com.yangda.example.entity.request.BaseRequest;


/**
 * 角色权限entity
 * 
 * @author ty
 * @date 2015年1月13日
 */
public class RolePermissionRequest extends BaseRequest {
	// Fields
	private String permissionIds;
	private Integer roleId;

	// Constructors

	/** default constructor */
	public RolePermissionRequest() {
	}

	public String getPermissionIds() {
		return permissionIds;
	}

	public void setPermissionIds(String permissionIds) {
		this.permissionIds = permissionIds;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

}