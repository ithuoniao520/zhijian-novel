package com.yangda.example.system.service;

import com.yangda.example.interfaces.IBaseService;
import com.yangda.example.system.entity.User;


public interface IUserService extends IBaseService <User,Integer>{

}
