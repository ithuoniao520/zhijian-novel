package com.yangda.example.system.dao;

import com.yangda.example.interfaces.IBaseDao;
import com.yangda.example.system.entity.Permission;

public interface IPermissionDao  extends IBaseDao<Permission ,Integer>{

}
