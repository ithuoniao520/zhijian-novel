package com.yangda.example.system.dao.impl;

import com.yangda.example.dao.BaseDao;
import com.yangda.example.system.dao.IUserRoleDao;
import com.yangda.example.system.entity.UserRole;
import org.springframework.stereotype.Repository;



@Repository
public class UserRoleDao extends BaseDao<UserRole, Integer> implements IUserRoleDao{


}
