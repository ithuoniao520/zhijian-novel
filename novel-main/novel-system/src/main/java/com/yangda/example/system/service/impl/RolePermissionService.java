package com.yangda.example.system.service.impl;

import com.yangda.example.interfaces.IBaseDao;
import com.yangda.example.service.BaseService;
import com.yangda.example.system.dao.IRolePermissionDao;
import com.yangda.example.system.entity.RolePermission;
import com.yangda.example.system.service.IRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolePermissionService extends BaseService<RolePermission, Integer> implements IRolePermissionService {
	
	@Autowired
	private IRolePermissionDao rolePermissionDao;

	@Override
	public IBaseDao<RolePermission, Integer> getEntityDao() {
		return rolePermissionDao;
	}
}
