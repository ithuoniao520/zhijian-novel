package com.yangda.example.system.dao;

import com.yangda.example.interfaces.IBaseDao;
import com.yangda.example.system.entity.User;


public interface IUserDao  extends IBaseDao<User ,Integer>{
}
