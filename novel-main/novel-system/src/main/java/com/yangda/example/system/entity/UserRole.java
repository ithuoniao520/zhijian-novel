package com.yangda.example.system.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.alibaba.fastjson.annotation.JSONField;
import com.yangda.example.entity.BaseEntity;

/**
 * 用户角色entity
 */
@Entity(name = "sys_user_role")
@DynamicInsert
@DynamicUpdate
public class UserRole extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JSONField(serialize = false)
    private User user;
    @ManyToOne(fetch = FetchType.LAZY)
    private Role role;

    /**
     * default constructor
     */
    public UserRole() {
    }

    /**
     * full constructor
     */
    public UserRole(User user, Role role) {
        this.user = user;
        this.role = role;
    }


    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

}