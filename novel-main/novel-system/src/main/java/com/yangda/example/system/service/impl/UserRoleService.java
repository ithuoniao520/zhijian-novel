package com.yangda.example.system.service.impl;

import com.yangda.example.interfaces.IBaseDao;
import com.yangda.example.service.BaseService;
import com.yangda.example.system.dao.IUserRoleDao;
import com.yangda.example.system.entity.UserRole;
import com.yangda.example.system.service.IUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService extends BaseService<UserRole, Integer> implements IUserRoleService {

    @Autowired
    private IUserRoleDao userRoleDao;

    @Override
    public IBaseDao<UserRole, Integer> getEntityDao() {
        return userRoleDao;
    }

}
