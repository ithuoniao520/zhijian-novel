package com.yangda.example.system.service.impl;

import com.yangda.example.interfaces.IBaseDao;
import com.yangda.example.service.BaseService;
import com.yangda.example.system.dao.IUserDao;
import com.yangda.example.system.entity.User;
import com.yangda.example.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService extends BaseService<User, Integer> implements IUserService {

    /**
     * 加密方法
     */
    public static final String HASH_ALGORITHM = "SHA-1";
    public static final int HASH_INTERATIONS = 1024;
    private static final int SALT_SIZE = 8;    //盐长度

    @Autowired
    private IUserDao userDao;

    @Override
    public IBaseDao<User, Integer> getEntityDao() {
        return userDao;
    }

}
