package com.yangda.example.system.dao.impl;

import com.yangda.example.dao.BaseDao;
import com.yangda.example.system.dao.IRolePermissionDao;
import com.yangda.example.system.entity.RolePermission;
import org.springframework.stereotype.Repository;

@Repository
public class RolePermissionDao extends BaseDao<RolePermission, Integer> implements IRolePermissionDao {

}
