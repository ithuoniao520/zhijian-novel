package com.yangda.example.system.dto;

import java.io.Serializable;

/**
 * 
 * @文件名 :Node.java
 * @创建人 :carl
 * @时间  :2016年8月9日 下午5:26:05 
 * @描述  :菜单
 */
public class Node implements Serializable {
	
	/**
	 * ID
	 */
	private Integer id;
	
	/**
	 * 父类ID
	 */
	private Integer pId;
	
	/**
	 * 名称
	 */
	private String name;
	
	private boolean open = true;  

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getpId() {
		return pId;
	}

	public void setpId(Integer pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
}
