package com.yangda.example.system.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.yangda.example.entity.BaseEntity;


/**
 * 权限 entity
 */

@Entity(name = "sys_permission")
@DynamicInsert
@DynamicUpdate
public class Permission extends BaseEntity {

    @Column(name = "pid")
    private Integer pid;//菜单父ID
    @Column(name = "name", nullable = false, length = 50)
    private String name;//菜单名称
    @Column(name = "type", length = 20)
    private String type;//类型
    @Column(name = "sequence")
    private Integer sequence;//排序
    @Column(name = "url")
    private String url;//菜单链接
    @Column(name = "icon")
    private String icon;//菜单图标
    @Column(name = "code", length = 50)
    private String code;//权限编码
    @Column(name = "state", length = 1)
    private String state;//菜单状态
    @Column(name = "isfixed", length = 1)
    private Integer isfixed;//0 不固定 1  固定
    @Column(name = "remark", length = 200)
    private String remark;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "permission")
    private Set<RolePermission> rolePermissions = new HashSet<RolePermission>(0);

    /**
     * default constructor
     */
    public Permission() {
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getIsfixed() {
        return isfixed;
    }

    public void setIsfixed(Integer isfixed) {
        this.isfixed = isfixed;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Set<RolePermission> getRolePermissions() {
        return rolePermissions;
    }

    public void setRolePermissions(Set<RolePermission> rolePermissions) {
        this.rolePermissions = rolePermissions;
    }
}