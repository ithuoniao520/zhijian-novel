package com.yangda.example.system.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.yangda.example.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * 用户
 */
@Entity(name = "sys_user")
@DynamicInsert
@DynamicUpdate
public class User extends BaseEntity {

    /***
     * 登录名
     */
    @Column(name = "login_id", unique = true, nullable = false, length = 20)
    private String loginId;

    /***
     * 登录密码
     */
    @Column(name = "password", nullable = false)
    private String password;

    /***
     * 用户姓名
     */
    @Column(name = "name", length = 40)
    private String name;

    /***
     * 邮箱
     */
    @Column(name = "mail", length = 40)
    private String mail;

    /***
     * 手机号
     */
    @Column(name = "phone", length = 20)
    private String phone;

    @Column(name = "salt")
    private String salt;

    /***
     * 用户状态，启用=0，禁用=1
     */
    @Column(name = "status", nullable = false)
    private int status;

    @Column(name = "remark", length = 200)
    private String remark;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    @JSONField(serialize = false)
    private Set<UserRole> userRoles = new HashSet<UserRole>(0);

    /**
     * default constructor
     */
    public User() {
    }

    public User(Integer id) {
        super(id);
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }
}