package com.yangda.example.system.dto;

import com.yangda.example.entity.request.BaseRequest;


public class MaintainerRequest extends BaseRequest {
	
	private String areaIds;//片区ID
	private String userId;//申报人
	private String userCode;//学工号
	
	
	
	public MaintainerRequest() {
		
	}
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAreaIds() {
		return areaIds;
	}
	public void setAreaIds(String areaIds) {
		this.areaIds = areaIds;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	
	
}
