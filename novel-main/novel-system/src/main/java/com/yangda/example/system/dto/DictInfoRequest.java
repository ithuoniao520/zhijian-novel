package com.yangda.example.system.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.yangda.example.entity.request.BaseRequest;

/**
 * 
 * @文件名 :DictInfoRequest.java
 * @创建人 :carl
 * @时间  :2016年8月9日 下午4:15:04 
 * @描述  :接收参数实体
 */
public class DictInfoRequest extends BaseRequest {
	
	@JSONField(name = "_parentId")
	private Integer _parentId;// easyiui中指定其父节点
	private Integer pId;
	protected Integer orderNo;
	/**
	 * 字典key
	 */
	private String codeKey;
	
	/**
	 * 字典value
	 */
	private String codeValue;
	
	private String name;
	
	/**
	 * 字典父类
	 */
	private String parentCodeKey;

	
	public String getCodeValue() {
		return codeValue;
	}

	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	public String getParentCodeKey() {
		return parentCodeKey;
	}

	public void setParentCodeKey(String parentCodeKey) {
		this.parentCodeKey = parentCodeKey;
	}

	public String getCodeKey() {
		return codeKey;
	}

	public void setCodeKey(String codeKey) {
		this.codeKey = codeKey;
	}

	public Integer get_parentId() {
		return _parentId;
	}

	public void set_parentId(Integer _parentId) {
		this._parentId = _parentId;
	}

	public Integer getpId() {
		return pId;
	}

	public void setpId(Integer pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	
	
}
