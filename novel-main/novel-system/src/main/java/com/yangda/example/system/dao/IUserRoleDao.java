package com.yangda.example.system.dao;

import com.yangda.example.interfaces.IBaseDao;
import com.yangda.example.system.entity.UserRole;


public interface IUserRoleDao  extends IBaseDao<UserRole ,Integer>{

}
