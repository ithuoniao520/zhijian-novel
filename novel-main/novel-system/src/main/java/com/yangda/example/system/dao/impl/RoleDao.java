package com.yangda.example.system.dao.impl;

import com.yangda.example.dao.BaseDao;
import com.yangda.example.system.dao.IRoleDao;
import com.yangda.example.system.entity.Role;
import org.springframework.stereotype.Repository;


@Repository
public class RoleDao extends BaseDao<Role, Integer>  implements IRoleDao{

}
