package com.yangda.example.system.entity;

import com.yangda.example.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


/**
 * 角色entity
 */
@Entity(name = "sys_role")
@DynamicInsert
@DynamicUpdate
public class Role extends BaseEntity {

    @Column(name = "name", nullable = false, length = 20)
    private String name;
    @Column(name = "code", unique = true, nullable = false, length = 40)
    private String code;
    @Column(name = "sort")
    private Short sort;
    @Column(name = "isfixed", length = 1)
    private Integer isfixed;//0 不固定 1  固定
    @Column(name = "remark", length = 200)
    private String remark;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "role")
    private Set<UserRole> userRoles = new HashSet<UserRole>(0);
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "role")
    private Set<RolePermission> rolePermissions = new HashSet<RolePermission>(0);

    /**
     * default constructor
     */
    public Role() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Short getSort() {
        return sort;
    }

    public void setSort(Short sort) {
        this.sort = sort;
    }

    public Integer getIsfixed() {
        return isfixed;
    }

    public void setIsfixed(Integer isfixed) {
        this.isfixed = isfixed;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public Set<RolePermission> getRolePermissions() {
        return rolePermissions;
    }

    public void setRolePermissions(Set<RolePermission> rolePermissions) {
        this.rolePermissions = rolePermissions;
    }
}