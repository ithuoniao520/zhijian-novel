package com.yangda.example.system.entity;

import com.yangda.example.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;


/**
 * 角色权限entity
 */
@Entity(name = "sys_role_permission")
@DynamicInsert
@DynamicUpdate
public class RolePermission extends BaseEntity {

    private static final long serialVersionUID = 1L;
    @ManyToOne(fetch = FetchType.LAZY)
    private Permission permission;
    @ManyToOne(fetch = FetchType.LAZY)
    private Role role;

    /**
     * default constructor
     */
    public RolePermission() {
    }

    public Permission getPermission() {
        return this.permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

}