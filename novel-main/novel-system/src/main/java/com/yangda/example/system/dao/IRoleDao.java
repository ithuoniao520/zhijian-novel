package com.yangda.example.system.dao;

import com.yangda.example.system.entity.Role;
import com.yangda.example.interfaces.IBaseDao;


public interface IRoleDao  extends IBaseDao<Role ,Integer>{

}
