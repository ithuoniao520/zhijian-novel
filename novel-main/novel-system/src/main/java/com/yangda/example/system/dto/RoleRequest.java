package com.yangda.example.system.dto;

import com.yangda.example.entity.request.BaseRequest;


public class RoleRequest extends BaseRequest {

	// Fields
	private static final long serialVersionUID = 1L;
	private String name;
	private String roleCode;
	private Short sort;
	private String delFlag;
	private String keyword;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoleCode() {
		return this.roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public Short getSort() {
		return this.sort;
	}

	public void setSort(Short sort) {
		this.sort = sort;
	}

	public String getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	

}
