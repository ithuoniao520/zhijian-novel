package com.yangda.example.system.service.impl;

import com.yangda.example.interfaces.IBaseDao;
import com.yangda.example.service.BaseService;
import com.yangda.example.system.dao.IPermissionDao;
import com.yangda.example.system.entity.Permission;
import com.yangda.example.system.service.IPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PermissionService extends BaseService<Permission, Integer> implements IPermissionService{

	@Autowired
	private IPermissionDao permissionDao;

	@Override
	public IBaseDao<Permission, Integer> getEntityDao() {
		return permissionDao;
	}
}
