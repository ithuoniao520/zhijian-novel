package com.yangda.example.system.dao.impl;

import com.yangda.example.dao.BaseDao;
import com.yangda.example.system.dao.IPermissionDao;
import com.yangda.example.system.entity.Permission;
import org.springframework.stereotype.Repository;


@Repository
public class PermissionDao extends BaseDao<Permission, Integer>  implements IPermissionDao{

}
