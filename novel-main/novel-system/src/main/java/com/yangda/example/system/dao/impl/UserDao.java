package com.yangda.example.system.dao.impl;

import com.yangda.example.dao.BaseDao;
import com.yangda.example.system.dao.IUserDao;
import com.yangda.example.system.entity.User;
import org.springframework.stereotype.Repository;


@Repository
public class UserDao extends BaseDao<User, Integer>  implements IUserDao{

}
