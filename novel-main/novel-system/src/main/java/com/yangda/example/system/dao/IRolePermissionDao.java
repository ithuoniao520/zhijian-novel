package com.yangda.example.system.dao;

import com.yangda.example.interfaces.IBaseDao;
import com.yangda.example.system.entity.RolePermission;


public interface IRolePermissionDao extends IBaseDao<RolePermission, Integer> {
}
