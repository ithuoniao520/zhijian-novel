package com.yangda.example.interfaces;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.yangda.example.framework.model.PageRequest;
import com.yangda.example.framework.model.Pagination;
import org.hibernate.criterion.Criterion;

public interface IBaseService<T, PK extends Serializable> {

	/**
	 * 子类需要实现该方法，提供注入的dao
	 * @return
	 */
	IBaseDao<T, PK> getEntityDao();
	
	T get(final PK id) ;

	void save(final T entity);
	
	void update(final T entity);
	
	void delete(final T entity);
	
	void delete(final PK id);

	void batchDelete(PK[] ids);
	
	List<T> getAll();

	List<T> getAll(String orderByProperty, boolean isAsc);
	
	Pagination<T> findPage(final String hql, PageRequest pageRequest, final Map<String, ?> values) ;

	Pagination<T> findPage(PageRequest pageRequest, final Criterion... criterions);
	
	T  findUniqueBy(final String propertyName, final Object value) ;

	
}
