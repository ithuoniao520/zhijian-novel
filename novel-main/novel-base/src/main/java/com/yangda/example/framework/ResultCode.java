package com.yangda.example.framework;


/**
 * 业务码
 *
 * @author zhangw
 */
public enum ResultCode {

    SCUCESS(2000, "成功"),
    SYSTEM_ERROR(5000, "系统错误"),
    DATA_EMPTY(4000, "数据不存在"),
    DATA_EXISTED(4001, "数据已存在"),
    PARAMS_ERROR(4002, "参数验证失败"),
    TOKEN_ERROR(4003, "表单重复提交失败"),
    PERMISSION_ERROR(4004, "权限验证失败"),
    DEPENDENCY_ERROR(4005, "存在数据依赖"),


    //业务异常			1000-1999
    CAPTCHA_ERROR(1000, "验证码错误"),
    ROLE_CODE_EXISTED(1001, "角色编码已存在"),
    LOGIN_ERROR(1002, "学工号或密码错误"),
    PASSWORD_ERROR(1003, "密码验证错误"),
    CODEKEY_EXIST(1004, "该键已存在，保存失败"),
    DUPLICATE_ERROR(1005, "重复操作"),
    NAME_EXIST(1006, "该用户名已存在，保存失败");

    // 成员变量
    private int code;
    private String message;

    // 构造方法
    private ResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    // 覆盖方法
    @Override
    public String toString() {
        return this.code + "_" + this.message;
    }
}
