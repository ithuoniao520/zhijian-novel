package com.yangda.example.interfaces;

import com.yangda.example.framework.model.Pagination;
import com.yangda.example.framework.model.PageRequest;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * @文件名 :IBaseDao.java
 */

public interface IBaseDao<T, PK extends Serializable> {

    String getIdName();

    Session getSession();

    void save(final T entity);

    void delete(final T entity);

    void delete(final PK id);

    void batchDelete(PK[] ids);

    int batchExecute(final String hql, final Object... values);

    int batchExecute(final String hql, final Map values);

    T find(final PK id);

    <X> X findUniqueBy(final String propertyName, final Object value);

    <X> X findUnique(final String hql, final Map values);

    <X> X findUnique(final Criterion... criterions);

    List<T> find(final Collection<PK> idList);

    List<T> findAll();

    List<T> findAll(String orderByProperty, boolean isAsc);

    List<T> find(final String hql, final Map values);

    List<T> find(final String propertyName, final Object value);

    List<T> find(final Criterion... criterions);

    List query(final String sql, final Map values, Class clazz);

    Object queryUnique(final String sql, final Map values, Class clazz);

    Pagination findPage(final String hql, PageRequest pageRequest, final Map values, Class clazz);

    Pagination<T> findPage(final String hql, PageRequest pageRequest, final Map values);

    Pagination<T> findPage(PageRequest pageRequest, final Criterion... criterions);

    Pagination query(final String sql, PageRequest pageRequest, final Map values, Class clazz);
}
