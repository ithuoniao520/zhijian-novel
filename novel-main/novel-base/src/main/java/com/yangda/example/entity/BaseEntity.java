package com.yangda.example.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * @文件名 :BaseEntity.java
 * @创建人 :carl
 * @时间 :2016年7月28日 下午5:58:29
 * @描述 :所有实体的超类
 */
@MappedSuperclass
public class BaseEntity extends PK {

    private static final long serialVersionUID = 1L;

    public BaseEntity(Integer id) {
        super(id);
    }

    public BaseEntity() {
        super();
    }

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @Temporal(TemporalType.TIMESTAMP)
    @org.hibernate.annotations.CreationTimestamp
    protected Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    @Temporal(TemporalType.TIMESTAMP)
    @org.hibernate.annotations.UpdateTimestamp
    protected Date updateTime;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}
