package com.yangda.example.framework.model;

import java.io.Serializable;

/**
 * 分页请求参数
 */
public class PageRequest implements Serializable {

    /**
     * @Fields serialVersionUID :
     */
    private static final long serialVersionUID = 8865708753876370360L;

    //-- 公共变量 --//
    public static final String ASC = "asc";
    public static final String DESC = "desc";

    /*** 当前页码*/
    private Integer currentPage = 1;
    /*** 每页数量*/
    private Integer pageSize = 25;
    /*** 排序顺序 asc desc*/
    private String order;// asc desc
    /*** 排序字段的名称*/
    private String orderBy;

    public PageRequest() {
    }

    public PageRequest(Integer currentPage, Integer pageSize) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public PageRequest(Integer currentPage, Integer pageSize, String order, String orderBy) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.order = order;
        this.orderBy = orderBy;
    }
}
