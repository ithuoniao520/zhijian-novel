package com.yangda.example.framework.model;

import java.io.Serializable;
import java.util.List;

/**
* @Title: Pagination.java
* @Package com.yangda.entity 
* @Description: 分页数据结果
* @author wuhao
* @date 2015-8-2 
* @version V1.0
 */
public class Pagination<T> implements Serializable {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = -1311289985216629801L;

	/*** 总数量 */
	private Integer totalCount = 0;
	/*** 总页数 */
	private Integer totalPage = 0;
	/*** 每页显示数量 */
	private Integer pageSize = 0;
	/*** 当前页码 */
	private Integer currentPage=0;
	/*** 是否有下一页 */
	private Boolean hasPrevPage;
	/*** 是否有上一页 */
	private Boolean hasNextPage;
	
	/*** 数据结果集 */
	private List<T> result;

	public Pagination() {
		super();
	}


	public Pagination(Integer totalCount, Integer totalPage, Integer pageSize, Integer currentPage, Boolean hasPrevPage,
					  Boolean hasNextPage, List<T> result) {
		super();
		this.totalCount = totalCount;
		this.totalPage = totalPage;
		this.pageSize = pageSize;
		this.currentPage = currentPage;
		this.hasPrevPage = hasPrevPage;
		this.hasNextPage = hasNextPage;
		this.result = result;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getTotalPage() {
//		return (totalCount - 1) / pageSize + 1;
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Boolean getHasPrevPage() {
		return hasPrevPage;
//		return currentPage == 1 ? false : true;
	}

	public void setHasPrevPage(Boolean hasPrevPage) {
		this.hasPrevPage = hasPrevPage;
	}

	public Boolean getHasNextPage() {
//		return currentPage == totalPage || totalPage == 0 ? false : true;
		return hasNextPage;
	}

	public void setHasNextPage(Boolean hasNextPage) {
		this.hasNextPage = hasNextPage;
	}

	public List<T> getResult() {
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

}
