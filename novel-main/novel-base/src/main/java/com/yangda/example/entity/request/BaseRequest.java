package com.yangda.example.entity.request;

import java.util.Date;

import com.yangda.example.framework.model.PageRequest;

/**
 * 请求实体基类
 */
public class BaseRequest {

    protected Integer id;

    protected Date createTime;

    protected Date updateTime;

    protected PageRequest pageRequest = new PageRequest();

    public BaseRequest() {
        super();
    }

    public BaseRequest(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PageRequest getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(PageRequest pageRequest) {
        this.pageRequest = pageRequest;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "BaseRequest{" +
                "id=" + id +
                ", pageRequest=" + pageRequest +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
