package com.yangda.example.entity.response;

import java.util.Date;

/**
 * 响应实体基类
 */
public class BaseResponse {

    protected Integer id;

    protected Date createTime;

    private Date updateTime;

    public BaseResponse() {
        super();
    }

    public BaseResponse(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}
