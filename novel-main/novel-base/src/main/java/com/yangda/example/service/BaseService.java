package com.yangda.example.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.yangda.example.framework.model.Pagination;
import com.yangda.example.framework.model.PageRequest;
import org.hibernate.criterion.Criterion;
import org.springframework.transaction.annotation.Transactional;

import com.yangda.example.interfaces.IBaseDao;
import com.yangda.example.interfaces.IBaseService;

public abstract class BaseService<T,PK extends Serializable> implements IBaseService<T, PK> {

	/**
	 * 子类需要实现该方法，提供注入的dao
	 * @return
	 */
	public abstract IBaseDao<T, PK> getEntityDao();
	
	
	@Transactional(readOnly = true)
	public T get(final PK id) {
		return getEntityDao().find(id);
	}

	@Transactional(readOnly =false )
	public void save(final T entity) {
		getEntityDao().save(entity);
	}
	
	@Transactional(readOnly = false)
	public void update(final T entity){
		getEntityDao().save(entity);
	}
	
	@Transactional(readOnly = false)
	public void delete(final T entity){
		getEntityDao().delete(entity);
	}
	
	@Transactional(readOnly = false)
	public void delete(final PK id){
		getEntityDao().delete(id);
	}

	@Override
	@Transactional(readOnly = false)
	public void batchDelete(PK[] ids) {
		getEntityDao().batchDelete(ids);
	}

	@Transactional(readOnly = true)
	public List<T> getAll(){
		return getEntityDao().findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> getAll(String orderByProperty, boolean isAsc) {
		return getEntityDao().findAll(orderByProperty, isAsc);
	}

	@Transactional(readOnly = true)
	public Pagination findPage(final String hql, PageRequest pageRequest, final Map<String, ?> values) {
		return getEntityDao().findPage(hql, pageRequest, values);
	}

	@Transactional(readOnly = true)
	public Pagination findPage(PageRequest pageRequest, final Criterion... criterions){
		return getEntityDao().findPage(pageRequest, criterions);
	}

	@Transactional(readOnly = true)
	public T  findUniqueBy(final String propertyName, final Object value) {
		return getEntityDao().findUniqueBy(propertyName, value);
	}
}
