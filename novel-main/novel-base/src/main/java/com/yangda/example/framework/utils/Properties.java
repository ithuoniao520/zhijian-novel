package com.yangda.example.framework.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

/**
 * Created by carl on 2016/9/18.
 * 读取配置文件
 */
@Repository
public class Properties {

    /**
     * rsa开关
     */
    @Value("#{propertiesReader['rsa.switch']}")
    private boolean rsaSwitch;

    @Value("#{propertiesReader['school.http']}")
    private String schoolHttp;

    @Value("#{propertiesReader['school.port']}")
    private String schoolPort;

    public boolean getRsaSwitch() {
        return rsaSwitch;
    }

    public String getSchoolPort() {
        return schoolPort;
    }

    public void setSchoolPort(String schoolPort) {
        this.schoolPort = schoolPort;
    }

    public void setRsaSwitch(boolean rsaSwitch) {
        this.rsaSwitch = rsaSwitch;
    }

    public String getSchoolHttp() {
        return schoolHttp;
    }

    public void setSchoolHttp(String schoolHttp) {
        this.schoolHttp = schoolHttp;
    }

    /**
     * 报表地址
     *//*
    @Value("#{propertiesReader['report.http']}")
    private String reportHttp;

    *//**
     * 报表端口
     *//*
    @Value("#{propertiesReader['report.port']}")
    private String reportPort;

//    @Value("#{propertiesReader['school.http']}")
//    private String schoolHttp;

    @Value("#{propertiesReader['school.port']}")
    private String schoolPort;


    public String getSchoolHttp() {
        return schoolHttp;
    }

    public void setSchoolHttp(String schoolHttp) {
        this.schoolHttp = schoolHttp;
    }

    public String getSchoolPort() {
        return schoolPort;
    }

    public void setSchoolPort(String schoolPort) {
        this.schoolPort = schoolPort;
    }

    public String getReportHttp() {
        return reportHttp;
    }

    public void setReportHttp(String reportHttp) {
        this.reportHttp = reportHttp;
    }

    public String getReportPort() {
        return reportPort;
    }

    public void setReportPort(String reportPort) {
        this.reportPort = reportPort;
    }*/
}
