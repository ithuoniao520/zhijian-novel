package com.yangda.example.framework.exception;

import com.yangda.example.framework.ResultCode;

/**
* @Title: BizException.java
* @Package com.yangda.exception 
* @Description: 自定义业务异常基础类
* @author wuhao
* @date 2015-10-14 
* @version V1.0
 */
public class BizException extends RuntimeException {

	private static final long serialVersionUID = 8343048459443313229L;
	
	private Integer errorCode;
	
	private String title = "operation failed";
	
	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BizException() {
		super();
	}
	
	public BizException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public BizException(Integer errorCode, String message) {
		this(message);
		setErrorCode(errorCode);
	}
	
	public BizException(Integer errorCode, String title, String message) {
		this(message);
		setErrorCode(errorCode);
		setTitle(title);
	}
	
	public BizException(ResultCode resultCode) {
		this(resultCode.getMessage());
		setErrorCode(resultCode.getCode());
		setTitle(resultCode.getMessage());
	}

	public BizException(Integer errorCode, String message, Throwable cause) {
		this(message, cause);
		setErrorCode(errorCode);
	}
	
	public BizException(String message) {
		super(message);
	}

	public BizException(Throwable cause) {
	    super(cause);
	}
	
	
	@Override
	public String toString() {
		Integer errorCode = getErrorCode();
		String s = (errorCode != null) ? errorCode + "--" + getClass().getName() : getClass().getName();
        String message = getLocalizedMessage();
        return (message != null) ? (s + ": " + message) : s;
	}
	
}
