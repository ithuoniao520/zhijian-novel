package com.yangda.example.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

/**
 * 
 * @文件名 :PK.java
 * @创建人 :carl
 * @时间 :2016年7月28日 下午5:59:51
 * @描述 :主键
 */

@MappedSuperclass
public class PK implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	// @TableGenerator(name="tableGenerator",initialValue=1,allocationSize=1)
	@Column(name = "id")
	// @SequenceGenerator(
	// name = “generator”,//定义名为generator的生成策略
	// allocationSize = 1，//每次sequence加1
	// name=“seq_a”//引用名为seq_a的sequence
	// )
	@SequenceGenerator(allocationSize = 1, name = "seq_a")
	private Integer id;

	public PK() {

	}

	public PK(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
