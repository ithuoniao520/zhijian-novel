package com.yangda.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Hao on 2016-10-14.
 */

@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/create")
    @ResponseBody
    public String add(){
        return "";
    }
}
