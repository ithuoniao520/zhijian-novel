package com.yangda.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/")
@Controller
public class IndexController {

    /**
     * 登录页面跳转
     *
     * @return
     */
    @RequestMapping("/login")
    public String login() {
        return "/login/login";
    }

    @RequestMapping("/index")
    public String index() {
        return "/index";
    }
}
