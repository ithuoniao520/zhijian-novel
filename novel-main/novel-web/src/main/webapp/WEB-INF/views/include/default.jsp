<!--jquery begin-->
<script src="${ctxStatic}/plugins/jquery.min.js?v=2.1.4"></script>
<!--jquery end-->

<!--bootstrap begin-->
<link href="${ctxStatic}/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
<script src="${ctxStatic}/plugins/bootstrap.min.js?v=3.3.6"></script>
<link href="${ctxStatic}/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
<!--bootstrap end-->

<!--chosen begin-->
<link href="${ctxStatic}/plugins/chosen/chosen.css" rel="stylesheet">
<script src="${ctxStatic}/plugins/chosen/chosen.jquery.js"></script>
<!--chosen end-->

<!--bootstrap-table begin-->
<link href="${ctxStatic}/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
<link href="${ctxStatic}/plugins/bootstrap-table/bootstrap-table-fixed-columns.css" rel="stylesheet">

<script src="${ctxStatic}/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="${ctxStatic}/plugins/bootstrap-table/bootstrap-table-fixed-columns.js"></script>

<script src="${ctxStatic}/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
<!--bootstrap-table end-->

<!--bootstrap-suggest begin-->
<script src="${ctxStatic}/plugins/suggest/bootstrap-suggest.min.js"></script>
<!--bootstrap-suggest end-->

<!--layer begin-->
<script src="${ctxStatic}/plugins/layer/layer.js"></script>
<%-- <script src="${ctxStatic}/plugins/laydate/laydate.js"></script> --%>
<script src="${ctxStatic}/plugins/laydate/laydate.dev.js"></script>
<!--layer end-->

<!--jquery validate begin-->
<script src="${ctxStatic}/plugins/validate/jquery.validate.min.js"></script>
<script src="${ctxStatic}/plugins/validate/messages_zh.min.js"></script>
<script src="${ctxStatic}/plugins/validate/jquery.validate.extend.js"></script>
<!--jquery validate end-->


<!--artTemplate begin-->
<script src="${ctxStatic}/plugins/template/template.js"></script>
<!--artTemplate end-->


<!--jquery date format begin-->
<script src="${ctxStatic}/plugins/jquery-dateFormat.min.js"></script>
<!--jquery date format end-->

<!--ztree begin-->
<%-- <link href="${ctxStatic}/plugins/ztree/zTreeStyle/zTreeStyle.css" rel="stylesheet"> --%>
<link href="${ctxStatic}/plugins/ztree/metroStyle/metroStyle.css" rel="stylesheet">
<script src="${ctxStatic}/plugins/ztree/jquery.ztree.all.min.js"></script>
<!--ztree end-->

<!-- uploadify start -->
<link href="${ctxStatic}/plugins/uploadify/uploadify.css" rel="stylesheet">
<script src="${ctxStatic}/plugins/uploadify/jquery.uploadify.js"></script>
<!-- uploadify end -->

<!-- laypage start -->
<script src="${ctxStatic}/plugins/laypage/laypage/laypage.js"></script>
<!-- laypage end -->

<link href="${ctxStatic}/css/animate.min.css" rel="stylesheet">
<link href="${ctxStatic}/css/style.min862f.css?v=4.1.0" rel="stylesheet">

<!--utils/common begin-->
<script src="${ctxStatic}/js/map.js"></script>
<script src="${ctxStatic}/js/msg.js"></script>
<script src="${ctxStatic}/js/constant.js"></script>
<script src="${ctxStatic}/js/utils.js"></script>
<script src="${ctxStatic}/js/base.js"></script>
<!--utils/common end-->
<script type="text/javascript">
    //全局的AJAX访问，处理AJAX清求时SESSION超时
    /*$.ajaxSetup({
     contentType:"application/x-www-form-urlencoded;charset=utf-8",
     complete:function(XMLHttpRequest,textStatus){
     //通过XMLHttpRequest取得响应头，sessionstatus
     var sessionstatus=XMLHttpRequest.getResponseHeader("sessionstatus");
     if(sessionstatus=="timeout"){
     //跳转的登录页面
     window.location.replace('${ctx}/login/toLogin');
     }
     }
     });*/
</script>



