<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglibs.jsp"%>

<!DOCTYPE html>
<html>
<head>
<title>湖南大学游泳馆管理系统</title>
<%@ include file="/WEB-INF/views/include/default.jsp"%>
</head>

<body class="gray-bg">
<div class="middle-box text-center animated fadeInDown">
    <h1>404</h1>
    <h3 class="font-bold">页面未找到！</h3>

    <div class="error-desc">
        抱歉，页面好像去火星了~
        <br/><a href="${ctx}/index" class="btn btn-primary m-t">返回主页</a>
    </div>
</div>
</body>

</html>
