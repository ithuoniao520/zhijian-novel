package com.yangda.example.utils;

import com.alibaba.fastjson.JSON;
import net.sf.cglib.beans.BeanCopier;
import net.sf.cglib.core.Converter;
import org.dozer.DozerBeanMapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class BeanUtils {
	private static final String DOZER_CONFIG = "dozer-config/dozerBeanMapping.xml";
	private static DozerBeanMapper mapper;

	private BeanUtils() {
	}

	private static BeanUtils single = null;

	// 静态工厂方法
	public static BeanUtils getInstance() {
		if (single == null) {
			single = new BeanUtils();
		}
		return single;
	}

	static {
		mapper = new DozerBeanMapper();
		List<String> mappers = new ArrayList<String>();
		mappers.add("dozer-config/dozerBeanMapping.xml");
		mapper.setMappingFiles(mappers);
	}

	public static <T> T copy(Object source, Class<T> destinationClass) {
		return (T) mapper.map(source, destinationClass);
	}
	public static void clone(Object source, Object destination) {
		  mapper.map( source,  destination);
	}

	public static <T> List<T> copy(Collection sourceList, Class<T> destinationClass) {
		List<T> destinationList = new ArrayList<T>();

		for (Object sourceObject : sourceList) {
			T destinationObject = mapper.map(sourceObject, destinationClass);
			destinationList.add(destinationObject);
		}
		return destinationList;
	}


	private static final ConcurrentMap<String, BeanCopier> BEAN_COPIERS = new ConcurrentHashMap<String, BeanCopier>();

	public static BeanCopier create(Object source, Object target, boolean useConverter) {
		String key = genKey(source, target);
		BeanCopier copier = null;
		if (!BEAN_COPIERS.containsKey(key)) {
			try {
				copier = BeanCopier.create(source.getClass(), target.getClass(), useConverter);
				BEAN_COPIERS.put(key, copier);
			} catch (Exception e) {
//				System.out.println(e.getMessage());
			}
		} else {
			copier = BEAN_COPIERS.get(key);
		}

		return copier;
	}

	private static void copy(Object source, Object target) {
		BeanCopier copier = create(source, target, false);
		copier.copy(source, target, null);
	}

	/**
	 * 复制对象
	 * @param o
	 * @param calssz
	 * @return
	 */
	public static <T> T copyJSONObject(Object o,Class<T> calssz){
		String string = JSON.toJSONString(o);
		return JSON.parseObject(string, calssz);
	}
	
	/**
	 * 赋值list
	 * @param o
	 * @param calssz
	 * @return
	 */
	public static Object copyJSONList(Object o,Class calssz){
		String string = JSON.toJSONString(o);
		return JSON.parseArray(string, calssz);
	}
	
	/**
	 * @Description: TODO
	 * @param @param source 源集合
	 * @param @param target目标集合
	 * @param @param clazz 目标集合中的类
	 * @param @throws RuntimeException   
	 * @return void  
	 * @throws
	 * @author zhangwei
	 * @date 2015-10-16
	 */
	public static <T, E> void copy(List<T> source, List<E> target, Class<E> targetClazz)  throws RuntimeException { 
		for (int i = 0; i < source.size(); i++) {
			T src = source.get(i);
			E des = null;
			try {
				des = (E) targetClazz.newInstance();
			} catch (Exception e) {
				throw new RuntimeException(e.toString());
			}
			copy(src, des);
			target.add((E) des);
		}
	}

	public static void copy(Object source, Object target, Converter converter) {
		BeanCopier copier = create(source.getClass(), target.getClass(), true);
		copier.copy(source, target, converter);
	}

	private static String genKey(Object source, Object target) {
		return source.toString() + target.toString();
	}


}
