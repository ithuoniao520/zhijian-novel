package com.yangda.example.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.util.ResourceUtils;

public class PropertyUtils {

	// 配置文件获取文件
	private static PropertyUtils instance;
	public static String configUrl = "/config/application.properties";// 配置文件路径,监听器中初始化 InitializeListener
	private static Properties p;
	static{
		p = new Properties();
		InputStream inputStream = ResourceUtils.class.getClassLoader().getResourceAsStream(configUrl);
		try {
			p.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}

	/*private PropertyUtils() {
		p = new Properties();
		InputStream inputStream = ResourceUtils.class.getClassLoader().getResourceAsStream(configUrl);
		try {
			// String url = path+CONFIG;
			p.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// return p;
	}

	public static PropertyUtils getInstance() {
		if (instance == null) {
			instance = new PropertyUtils();
		}
		return instance;
	}*/

	public static String getValue(String key) {
		return p.getProperty(key.trim()).trim();
	}
	public static int getInt(String key) {
		return Integer.valueOf(p.getProperty(key.trim()).trim());
	}
}
