package com.yangda.example.utils;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @title: 时间工具类
 * @author: carl
 * @date: 2016年3月23日
 * @description:用于时间
 */
public class DateUtils {
	/**
	 * 时间格式 yyyy-MM-dd HH:mm:ss
	 */
	public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 时间格式 YYYYMMDDHHMMSS
	 * 
	 * @return
	 */
	public static final String YYYYMMDDHHMMSS = "yyyymmddhhmmss";

	/**
	 * 获取yyyymmddhhmmss
	 * 
	 * @return
	 */
	public static String getYYYYMMDDHHMMSS() {
		return conversion(YYYYMMDDHHMMSS);
	}

	public static String getDate(Date date) {
		return conversion(date, YYYY_MM_DD_HH_MM_SS);
	}

	/**
	 * 转换格式
	 * 
	 * @param format
	 * @return
	 */
	private static String conversion(String format) {
		return conversion(new Date(), format);
	}

	/**
	 * 转换格式
	 * 
	 * @param format
	 * @return
	 */
	private static String conversion(Date date, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(date);
	}

	public static String subSecond(Date date, int s) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.SECOND, s);
		date = c.getTime();
		return getDate(date);
	}

	/**
	 * 取得当前时间（包括日期和时间）
	 * 
	 * @return
	 */
	public static Date getCurrentDateTime() {
		Date date = new Date();
		return date;
	}

	/**
	 *  取得当前日期之后N天的日期 
	 * @param n
	 * @return
	 */
	public static Date afterNDay(int n) {
		Calendar c = Calendar.getInstance();
		//DateFormat df = new SimpleDateFormat(YYYYMMDDHHMMSS);
		c.setTime(new Date());
		c.add(Calendar.DATE, n);
		Date d2 = c.getTime();
		//String s = df.format(d2);
		return d2;
	}

	/**
	 * 计算两个日期之间相隔的天数，第1个时间>第2个时间时会返回负数
	 * @param d1 第一个时间
	 * @param d2 第二个时间
     * @return 相隔天数
     */
	public static Integer daysBetween(Date d1, Date d2){

		DateTime dt1 = new DateTime(d1);
		DateTime dt2 = new DateTime(d2);

		return Days.daysBetween(dt1, dt2).getDays();
	}
}
