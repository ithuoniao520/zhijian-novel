package com.yangda.example.utils;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * 自定义常用工具类
 * @author carl
 *
 */
public class Commons {

	/**
	 * 是否有效，有效：string!=null&&string.length>0，int,long>0，数组!=null&&size>0
	 * @param value
	 * @return true=值有效，false=值无效
     */
	public static boolean isValid(Object value){
		return !isInvalid(value);
	}

	/**
	 * 是否为空
	 * @return
	 */
	public static boolean isInvalid(Object value){
		if(value ==null){
			return true;
		}else if(value instanceof String){
			String instance = (String) value;
			if(null == value || instance.trim().length() <=0 || "null".equalsIgnoreCase(instance)){
				return true;
			}
		}else if(value instanceof Integer){
			Integer instance = (Integer) value;
			if(instance == null || instance < 0){
				return true;
			}
		}else if(value instanceof List<?>){
			List<?> instance = (List<?>) value;
			if(null == instance || instance.size() <= 0){
				return true;
			}
		}else if(value instanceof Map<?,?>){
			Map<?,?> instance = (Map<?,?>)value;
			if(null == instance || instance.size() <= 0){
				return true;
			}
		}else if(value instanceof Object[]){
			Object[] instance = (Object[])value;
			if(null == instance || instance.length <= 0){
				return true;
			}
		}else if(value instanceof Long){
			Long instance = (Long)value;
			if(null == instance || instance <= 0){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 类型转换
	 * @param value
	 * @return
	 */
	public static final int conversionType(String value){
		
		if(!isInvalid(value)){
			return Integer.parseInt(value);
		}
		return 0;
	}

	/**
	 * 根据分隔符把字符串转为Int数组
	 * @param str
	 * @param separator
     * @return
     */
	public static int[] str2ints(String str, String separator){
		if(str!=null && str.trim().length()>0){
			String temp = trimComma(str);
			String[] strs = StringUtils.split(temp, separator);
			if(strs!=null && strs.length>0){
				int[] ids = new int[strs.length];
				for (int i=0; i<strs.length; i++) {
					String s = strs[i];
					if(s!=null && s.trim().length()>0){
						ids[i] = Integer.parseInt(strs[i]);
					}
				}
				return ids;
			}
		}
		return null;
	}

	/**
	 * 逗号相隔的字符串转int数组
	 * @param str
	 * @return
     */
	public static int[] str2ints(String str){
		return str2ints(str, ",");
	}
	
	
	/**
	 * 逗号相隔的字符串转Integer数组
	 * @param str
	 * @return
	 */
	public static Integer[] str2Integers(String str) {
		return str2Integers(str, ",");
	}
	
	
	/**
	 * 根据分隔符把字符串转为Integer数组
	 * @param str
	 * @param separator
     * @return
     */
	public static Integer[] str2Integers(String str, String separator){
		if(str!=null && str.trim().length()>0){
			String temp = trimComma(str);
			String[] strs = StringUtils.split(temp, separator);
			if(strs!=null && strs.length>0){
				Integer[] ids = new Integer[strs.length];
				for (int i=0; i<strs.length; i++) {
					String s = strs[i];
					if(s!=null && s.trim().length()>0){
						ids[i] = Integer.parseInt(strs[i]);
					}
				}
				return ids;
			}
		}
		return null;
	}
	

	/**
	 * 去掉字符串首尾空格
	 * @param str
	 * @return
     */
	public static String trimComma(String str){
		String regex = "^,*|,*$";
		return str.replaceAll(regex, "");
	}

	public static void main(String[] args) {
		String str = ",1,156,,2";
//		System.out.printf(trimComma(str));
		System.out.println(str2ints(str).length);
	}
}
